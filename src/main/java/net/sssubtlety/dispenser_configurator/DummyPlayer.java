package net.sssubtlety.dispenser_configurator;

import com.mojang.authlib.GameProfile;
import io.netty.channel.*;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.entity.CommandBlockBlockEntity;
import net.minecraft.block.entity.SignBlockEntity;
import net.minecraft.class_7604;
import net.minecraft.command.argument.EntityAnchorArgumentType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.passive.HorseBaseEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.*;
import net.minecraft.network.chat.MessageSignature;
import net.minecraft.network.listener.PacketListener;
import net.minecraft.network.packet.c2s.play.*;
import net.minecraft.network.packet.s2c.play.PlayerPositionLookS2CPacket;
import net.minecraft.network.packet.s2c.play.SystemMessageS2CPacket;
import net.minecraft.recipe.Recipe;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.stat.Stat;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.unmapped.C_kwhcitou;
import net.minecraft.unmapped.C_tlvlkmlg;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.village.TradeOfferList;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.*;

public class DummyPlayer extends ServerPlayerEntity {
    private static final Text DUMMY_DISCONNECTION_REASON = Text.literal("DummyPlayer disconnection reason");
    private static final MutableText DUMMY_PLAYER_NAME = Text.literal("dummy_player");

    public DummyPlayer(World world) {
        super(world.getServer(), (ServerWorld) world, new GameProfile(makeUUID(world), null), null);
        //, new DummyServerPlayerInteractionManager((ServerWorld) world));
        this.networkHandler = new DummyServerPlayNetworkHandler(world.getServer(), this);
    }

    public static DummyPlayer createWithStackInMainHand(ItemStack stack, World world) {
        DummyPlayer dummyPlayer = new DummyPlayer(world);
        dummyPlayer.setStackInHand(Hand.MAIN_HAND, stack);

        return dummyPlayer;
    }

    private static UUID makeUUID(World world) {
        UUID dummyUUID;
        do {
            dummyUUID = UUID.randomUUID();
        } while(world.getPlayerByUuid(dummyUUID) != null);
        return dummyUUID;
    }

    // methods to change main inventory:
    // - giveItemStack

    //Override everything that's irrelevant to DummyPlayers
    @Override public void incrementStat(Stat<?> stat) { }
    @Override public void setExperiencePoints(int i) { }
    @Override public void setExperienceLevel(int level) { }
    @Override public void addExperienceLevels(int levels) { }
    @Override public void applyEnchantmentCosts(ItemStack enchantedItem, int experienceLevels) { }
    @Override public void onSpawn() { }
    @Override public void enterCombat() { }
    @Override public void endCombat() { }
    @Override public void tick() { }
    @Override public void playerTick() { }
    @Override public void onDeath(DamageSource source) { }
    @Override public void updateKilledAdvancementCriterion(Entity killer, int score, DamageSource damageSource) { }
    @Override public boolean damage(DamageSource source, float amount) { return false; }
    @Override public boolean shouldDamagePlayer(PlayerEntity player) { return false; }
    @Override public boolean canBeSpectated(ServerPlayerEntity spectator) { return false; }
//    @Override
//    public Either<SleepFailureReason, Unit> trySleep(BlockPos pos) {
//        return super.trySleep(pos).ifRight((unit) -> {
//            this.incrementStat(Stats.SLEEP_IN_BED);
//            Criterions.SLEPT_IN_BED.trigger(this);
//        });
//    }
    @Override public void sendPickup(Entity item, int count) { }
    @Override public void sleep(BlockPos pos) { }
    @Override public void wakeUp(boolean bl, boolean bl2) { }
    @Override public boolean startRiding(Entity entity, boolean force) { return false; }
    @Override public void stopRiding() { }
    @Override public void requestTeleportAndDismount(double destX, double destY, double destZ) { }
    @Override public boolean isInvulnerableTo(DamageSource damageSource) { return true; }
    @Override public void handleFall(double heightDifference, boolean onGround) { }
    @Override public void openEditSignScreen(SignBlockEntity signBlockEntity) { }
    @Override public OptionalInt openHandledScreen(NamedScreenHandlerFactory factory) { return OptionalInt.empty(); }
    @Override public void sendTradeOffers(int syncId, TradeOfferList offers, int levelProgress, int experience, boolean leveled, boolean refreshable) { }
    @Override public void openHorseInventory(HorseBaseEntity horseBaseEntity, Inventory inventory) { }
    @Override public void useBook(ItemStack book, Hand hand) { }
    @Override public void openCommandBlockScreen(CommandBlockBlockEntity commandBlockBlockEntity) { }
//    @Override public void onSlotUpdate(ScreenHandler handler, int slotId, ItemStack stack) { }
//    @Override public void onHandlerRegistered(ScreenHandler handler, DefaultedList<ItemStack> stacks) { }
    @Override public void closeHandledScreen() { }
    @Override public void closeScreenHandler() { }
    @Override public void updateInput(float sidewaysSpeed, float forwardSpeed, boolean jumping, boolean sneaking) { }
    @Override public void increaseStat(Stat<?> stat, int amount) { }
    @Override public void resetStat(Stat<?> stat) { }
    @Override public int unlockRecipes(Collection<Recipe<?>> recipes) { return 0; }
    @Override public void unlockRecipes(Identifier[] ids) { }
    @Override public int lockRecipes(Collection<Recipe<?>> recipes) { return 0; }
    @Override public void addExperience(int experience) { }
    @Override public void markHealthDirty() { }
    @Override public void sendMessage(Text message, boolean actionBar) { }
    // I use lookAt in ItemUseDelegate
//    @Override public void lookAt(EntityAnchorArgumentType.EntityAnchor anchorPoint, Vec3d target) {
//        // use LivingEntity lookAt to avoid accessing null PlayerInteractionManager
////        ((LivingEntityAccessor)this).callLookAt(anchorPoint, target);
//    }
    @Override public void lookAtEntity(EntityAnchorArgumentType.EntityAnchor anchorPoint, Entity targetEntity, EntityAnchorArgumentType.EntityAnchor targetAnchor) { }
    @Override public void copyFrom(ServerPlayerEntity oldPlayer, boolean alive) { }

    @Override
    public void requestTeleport(double destX, double destY, double destZ) {
        this.refreshPositionAndAngles(destX, destY, destZ, 0 ,0);
    }

    @Override public void refreshPositionAfterTeleport(double x, double y, double z)  { }
    @Override public void addCritParticles(Entity target) { }
    @Override public void addEnchantedHitParticles(Entity target) { }
    @Override public void sendAbilitiesUpdate() { }
    @Override public void sendSystemMessage(Text message) { }
    @Override public void method_43502(Text text, boolean bl) { }
    @Override public void method_43505(class_7604 arg, boolean bl, MessageType.C_iocvgdxe c_iocvgdxe) { }
    @Override public void method_44786(C_tlvlkmlg c_tlvlkmlg, MessageSignature messageSignature, byte[] bs) { }
    @Override public void setClientSettings(ClientSettingsC2SPacket packet) { }
    @Override public void sendResourcePackUrl(String url, String hash, boolean required, Text resourcePackPrompt) { }
//    @Override public void onStartedTracking(Entity entity) { }
    @Override public void updateLastActionTime() { }
    @Override public void setCameraEntity(Entity entity) { }
    @Override public void attack(Entity target) { }
    @Override public void swingHand(Hand hand) { }
//    @Override public void onTeleportationDone() { }
    @Override public void teleport(ServerWorld targetWorld, double x, double y, double z, float yaw, float pitch) { }
//    @Override public void setCameraPosition(ChunkSectionPos cameraPosition) { }

    //REQUIRED TO AVOID CRASH
    @Override public Text getName() {
        return DUMMY_PLAYER_NAME;
    }


    public static class Manager {
        private static final Set<DummyPlayer> availableDummies = new LinkedHashSet<>();

        public DummyPlayer getDummy(ServerWorld world) {
            final DummyPlayer dummy;
            if (availableDummies.isEmpty())
                dummy = new DummyPlayer(world);
            else {
                final Iterator<DummyPlayer> dummyItr = availableDummies.iterator();
                dummy = dummyItr.next();
                dummyItr.remove();
            }

            return dummy;
        }

        public void releaseDummy(DummyPlayer dummy) {
            dummy.getInventory().clear();
            if (!availableDummies.add(dummy))
                DispenserConfigurator.LOGGER.warn("Dummy player released multiple times, but should only be released once.");
        }
    }

//    private static class DummyServerPlayerInteractionManager extends ServerPlayerInteractionManager {
//        public DummyServerPlayerInteractionManager(ServerWorld world) {
//            super(world);
//        }
//
//        @Override public void setGameMode(GameMode gameMode) { }
//        @Override public void setGameMode(GameMode gameMode, GameMode gameMode2) { }
//        @Override public GameMode getGameMode() { return GameMode.SURVIVAL; }
//        @Override public boolean isSurvivalLike() { return true; }
//        @Override public boolean isCreative() { return false; }
//        @Override public void setGameModeIfNotPresent(GameMode gameMode) { }
//        @Override public void update() { }
//        @Override public void processBlockBreakingAction(BlockPos pos, PlayerActionC2SPacket.Action action, Direction direction, int worldHeight) { }
//        @Override public void finishMining(BlockPos pos, PlayerActionC2SPacket.Action action, String reason) { }
//        @Override public boolean tryBreakBlock(BlockPos pos) { return false; }
//        @Override public ActionResult interactItem(ServerPlayerEntity player, World world, ItemStack stack, Hand hand) { return ActionResult.PASS; }
//        @Override public ActionResult interactBlock(ServerPlayerEntity player, World world, ItemStack stack, Hand hand, BlockHitResult hitResult) { return ActionResult.PASS; }
//        @Override public void setWorld(ServerWorld world) { }
//    }

    private static class DummyServerPlayNetworkHandler extends ServerPlayNetworkHandler {
        public DummyServerPlayNetworkHandler(MinecraftServer server, ServerPlayerEntity player) {
            super(server, DummyClientConnection.DUMMY_CONNECTION, player);
        }

        @Override public void method_18784() { }
        @Override public void syncWithPlayerPosition() { }

        @Override
        public ClientConnection getConnection() { return DummyClientConnection.DUMMY_CONNECTION; }

        @Override public void disconnect(Text reason) { }
        @Override public void onPlayerInput(PlayerInputC2SPacket packet) { }
        @Override public void onVehicleMove(VehicleMoveC2SPacket packet) { }
        @Override public void onTeleportConfirm(TeleportConfirmC2SPacket packet) { }
        @Override public void onRecipeBookData(RecipeBookDataC2SPacket packet) { }
        @Override public void onRecipeCategoryOptions(RecipeCategoryOptionsC2SPacket packet) { }
        @Override public void onAdvancementTab(AdvancementTabC2SPacket packet) { }
        @Override public void onRequestCommandCompletions(RequestCommandCompletionsC2SPacket packet) { }
        @Override public void onUpdateCommandBlock(UpdateCommandBlockC2SPacket packet) { }
        @Override public void onUpdateCommandBlockMinecart(UpdateCommandBlockMinecartC2SPacket packet) { }
        @Override public void onPickFromInventory(PickFromInventoryC2SPacket packet) { }
        @Override public void onRenameItem(RenameItemC2SPacket packet) { }
        @Override public void onUpdateBeacon(UpdateBeaconC2SPacket packet) { }
        @Override public void onJigsawGenerating(JigsawGeneratingC2SPacket packet) { }
        @Override public void onBookUpdate(BookUpdateC2SPacket packet) { }
        @Override public void onQueryEntityNbt(QueryEntityNbtC2SPacket packet) { }
        @Override public void onQueryBlockNbt(QueryBlockNbtC2SPacket packet) { }
        @Override public void onPlayerMove(PlayerMoveC2SPacket packet) { }
        @Override public void requestTeleportAndDismount(double x, double y, double z, float yaw, float pitch) { }
        @Override public void requestTeleport(double x, double y, double z, float yaw, float pitch) { }
        @Override public void requestTeleport(double x, double y, double z, float yaw, float pitch, Set<PlayerPositionLookS2CPacket.Flag> flags) { }
        @Override public void requestTeleport(double x, double y, double z, float yaw, float pitch, Set<PlayerPositionLookS2CPacket.Flag> flags, boolean shouldDismount) { }
        @Override public void onPlayerAction(PlayerActionC2SPacket packet) { }
        @Override public void onPlayerInteractBlock(PlayerInteractBlockC2SPacket packet) { }
        @Override public void onPlayerInteractItem(PlayerInteractItemC2SPacket packet) { }
        @Override public void onSpectatorTeleport(SpectatorTeleportC2SPacket packet) { }
        @Override public void onResourcePackStatus(ResourcePackStatusC2SPacket packet) { }
        @Override public void onBoatPaddleState(BoatPaddleStateC2SPacket packet) { }
        @Override public void onDisconnected(Text reason) { }
        @Override public void sendPacket(Packet<?> packet) { }
        @Override public void method_14369(Packet<?> packet, @Nullable PacketSendListener packetSendListener) { }
        @Override public void onUpdateSelectedSlot(UpdateSelectedSlotC2SPacket packet) { }
        @Override public void onChatMessage(ChatMessageC2SPacket packet) { }
        @Override public void onHandSwing(HandSwingC2SPacket packet) { }
        @Override public void onClientCommand(ClientCommandC2SPacket packet) { }
        @Override public void onPlayerInteractEntity(PlayerInteractEntityC2SPacket packet) { }
        @Override public void onClientStatus(ClientStatusC2SPacket packet) { }
        @Override public void onCloseHandledScreen(CloseHandledScreenC2SPacket packet) { }
        @Override public void onClickSlot(ClickSlotC2SPacket packet) { }
        @Override public void onCraftRequest(CraftRequestC2SPacket packet) { }
        @Override public void onButtonClick(ButtonClickC2SPacket packet) { }
        @Override public void onCreativeInventoryAction(CreativeInventoryActionC2SPacket packet) { }
//        @Override public void onConfirmScreenAction(ConfirmScreenActionC2SPacket packet) { }
        @Override public void onKeepAlive(KeepAliveC2SPacket packet) { }
        @Override public void onClientSettings(ClientSettingsC2SPacket packet) { }
        @Override public void onCustomPayload(CustomPayloadC2SPacket packet) { }
        @Override public void onUpdateDifficulty(UpdateDifficultyC2SPacket packet) { }
        @Override public void onUpdateDifficultyLock(UpdateDifficultyLockC2SPacket packet) { }
    }

    private static class DummyClientConnection extends ClientConnection {
        public static final DummyClientConnection DUMMY_CONNECTION = new DummyClientConnection();
        private final PacketListener dummyListener;
        private static final InetSocketAddress DUMMY_ADDRESS = new InetSocketAddress(0);

        private DummyClientConnection() {
            super(NetworkSide.SERVERBOUND);
            dummyListener = new DummyPacketListener(this);
        }

        @Override public void channelActive(ChannelHandlerContext channelHandlerContext) { }
        @Override public void setState(NetworkState state) { }
        @Override public void channelInactive(ChannelHandlerContext channelHandlerContext) { }
        @Override public void exceptionCaught(ChannelHandlerContext channelHandlerContext, Throwable throwable) { }
        @Override protected void channelRead0(ChannelHandlerContext channelHandlerContext, Packet<?> packet) { }
        @Override public void setPacketListener(PacketListener listener) { }
        @Override public void send(Packet<?> packet) { }
        @Override public void send(Packet<?> packet, @Nullable PacketSendListener listener) { }
        @Override public void tick() { }

        @Override public SocketAddress getAddress() { return DUMMY_ADDRESS; }

        @Override public void disconnect(Text disconnectReason) { }
        @Override public boolean isLocal() { return false; }
//        @Environment(EnvType.CLIENT)
        @Override public boolean isEncrypted() { return false; }
        @Override public boolean isOpen() { return false; }
        @Override public boolean hasChannel() { return false; }

        @Override
        public PacketListener getPacketListener() { return this.dummyListener; }

        @Override
        public Text getDisconnectReason() {
            return DUMMY_DISCONNECTION_REASON;
        }

        @Override public void disableAutoRead() { }
        @Override public void handleDisconnection() { }
        @Environment(EnvType.CLIENT)
        @Override public float getAveragePacketsReceived() {
            return 0;
        }
        @Environment(EnvType.CLIENT)
        @Override public float getAveragePacketsSent() {
            return 0;
        }
    }

    private static class DummyPacketListener implements PacketListener {
        private final DummyClientConnection connection;
        DummyPacketListener(DummyClientConnection connection) { this.connection = connection; }

        @Override public void onDisconnected(Text reason) { }
        @Override public ClientConnection getConnection() { return this.connection; }
    }
}
