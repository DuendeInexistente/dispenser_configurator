package net.sssubtlety.dispenser_configurator;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.fabricmc.fabric.api.resource.ResourcePackActivationType;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;
import net.sssubtlety.dispenser_configurator.behavior.Configurator;
import net.sssubtlety.dispenser_configurator.behavior.delegate.block.BlockOnUseDelegate;
import net.sssubtlety.dispenser_configurator.behavior.delegate.block.ItemUseOnBlockDelegate;
import net.sssubtlety.dispenser_configurator.behavior.delegate.block_and_entity.DropDelegate;
import net.sssubtlety.dispenser_configurator.behavior.delegate.block_and_entity.FailDelegate;
import net.sssubtlety.dispenser_configurator.behavior.delegate.entity.EntityOnInteractDelegate;
import net.sssubtlety.dispenser_configurator.behavior.delegate.entity.ItemUseOnEntityDelegate;
import net.sssubtlety.dispenser_configurator.behavior.delegate.unpredicated.ItemUseDelegate;
import net.sssubtlety.dispenser_configurator.behavior.delegate.unpredicated.PlaceBlockDelegate;

public class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(Configurator.MANAGER);
        FabricLoader.getInstance().getModContainer(DispenserConfigurator.NAMESPACE).ifPresent(modContainer ->
                ResourceManagerHelper.registerBuiltinResourcePack(
                        new Identifier(DispenserConfigurator.NAMESPACE, "default_dispenser_configurators"),
                        modContainer,
                        ResourcePackActivationType.DEFAULT_ENABLED
                ));

        // reference each DispenserBehaviorDelegate class so they load
        PlaceBlockDelegate.init();
        BlockOnUseDelegate.init();
        ItemUseOnBlockDelegate.init();
        EntityOnInteractDelegate.init();
        ItemUseOnEntityDelegate.init();
        ItemUseDelegate.init();
        DropDelegate.init();
        FailDelegate.init();
    }
}
