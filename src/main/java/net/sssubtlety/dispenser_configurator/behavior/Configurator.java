package net.sssubtlety.dispenser_configurator.behavior;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonParseException;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.tag.TagKey;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.sssubtlety.dispenser_configurator.behavior.delegate.DispenserBehaviorDelegate;
import net.sssubtlety.dispenser_configurator.behavior.predicate.DualSet;
import net.sssubtlety.dispenser_configurator.behavior.predicate.universal.AllowSetUniversalAcceptor;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import static net.sssubtlety.dispenser_configurator.behavior.ConfiguratorManager.Keys;
import static net.sssubtlety.dispenser_configurator.DispenserConfigurator.LOGGER;

public class Configurator {
    public static final ConfiguratorManager MANAGER = new ConfiguratorManager("dispenser_configurators");

    public static final int MAX_PRIORITY = Integer.MAX_VALUE/2;
    public static final int MIN_PRIORITY = Integer.MIN_VALUE/2;

    public static final String GREATER_THAN_MAX_FALLBACK = " greater than maximum allowable priority. Using max priority instead: " + MAX_PRIORITY;
    public static final String LESS_THAN_MIN_FALLBACK = " less than minimum allowable priority. Using min priority instead: " + MIN_PRIORITY;

    public final Identifier id;
    public boolean exclusive;

    // only used during mapping
    public final int priority;

    private final List<DispenserBehaviorDelegate> delegates;

    private final Collection<Item> items;

    private final DualSet<Block> blockPredicate;
    private final DualSet<EntityType<?>> entityPredicate;

    private GenericDispenserBehavior resolvedBehavior;

    Configurator(Identifier id, int priority) {
        this.id = id;
        this.exclusive = false;
        this.priority = clampPriority(priority);

        this.delegates = new LinkedList<>();
        this.items = new LinkedList<>();

        this.blockPredicate = new DualSet<>();
        this.entityPredicate = new DualSet<>();
    }

    private int clampPriority(int priority) {
        if (priority > MAX_PRIORITY) {
            LOGGER.error("Trying to construct a Configurator (" + id + ") with" + GREATER_THAN_MAX_FALLBACK);
            return MAX_PRIORITY;
        } else if (priority < MIN_PRIORITY) {
            LOGGER.error("Trying to construct a Configurator (" + id + ") with" + LESS_THAN_MIN_FALLBACK);
            return MIN_PRIORITY;
        } else return priority;
    }

    protected Collection<Item> getItems() {
        return items;
    }

    protected Collection<Block> getBlockAllowSet() {
        return blockPredicate.allowSet;
    }
    protected Collection<Block> getBlockDenySet() {
        return blockPredicate.denySet;
    }
    protected Collection<EntityType<?>> getEntityAllowSet() {
        return entityPredicate.allowSet;
    }
    protected Collection<EntityType<?>> getEntityDenySet() {
        return entityPredicate.denySet;
    }

    public void addToListFromString(String listKey, String idString) {
        Objects.requireNonNull(KeyManager.keyMappings.get(listKey)).resolveIdString(this, idString);
    }

    public boolean intersects(Configurator other) {
        return this.blockPredicate.intersects(other.blockPredicate) || this.entityPredicate.intersects(other.entityPredicate);
    }

    public GenericDispenserBehavior resolveBehavior() {
        if (resolvedBehavior == null) resolvedBehavior = new GenericDispenserBehavior(delegates, blockPredicate, entityPredicate, id, exclusive);

        return resolvedBehavior;
    }

//    public boolean intersects(Configurator other) {
//        return (this.hasBlockBehavior && other.hasBlockBehavior && this.blockPredicate.intersects(other.blockPredicate)) ||
//                (this.hasEntityBehavior && other.hasEntityBehavior && this.entityPredicate.intersects(other.entityPredicate));
//    }

    // 0 means equal priority,
    //   positive means this has LOWER priority,
    //   negative means other has LOWER priority
    public int comparePriority(Configurator other) {
        // low priority if accepts a non-bounded set of items
        if (this.priority == other.priority) {
            boolean thisLow = isLowPriority();
            boolean otherLow = other.isLowPriority();

            return thisLow == otherLow ? 0 :
                            thisLow ? 1 : -1;
        } else {
            return  other.priority - this.priority;
        }
    }

    public boolean isLowPriority() {
        return this.blockPredicate.allowSet instanceof AllowSetUniversalAcceptor &&
                this.entityPredicate.allowSet instanceof AllowSetUniversalAcceptor;
    }

//    protected static Exception tagManagementException(Identifier id) {
//        return new JsonParseException("Error getting tag with id '" + id + "'");
//    }

    public void addDelegate(DispenserBehaviorDelegate delegate) {
        if (exclusive) throw new IllegalStateException("Trying to add unreachable delegate after an exclusive delegate.");
        else exclusive = delegate.isExclusive();
        delegates.add(delegate);
    }

    public List<String> getDelegateNames() {
        return delegates.stream().map(DispenserBehaviorDelegate::getName).collect(Collectors.toList());
    }

    protected static class KeyManager {
        private static final ImmutableMap<String, Keyed<?>> keyMappings;

        static {
            ImmutableMap.Builder<String, Keyed<?>> keyMappingsBuilder = ImmutableMap.builder();
            keyMappingsBuilder.put(Keys.items, new Keyed<>(Registry.ITEM, Configurator::getItems));

            mapAllToValue(Keys.block_allow_keys, new Keyed<>(Registry.BLOCK, Configurator::getBlockAllowSet), keyMappingsBuilder);
            mapAllToValue(Keys.block_deny_keys, new Keyed<>(Registry.BLOCK, Configurator::getBlockDenySet), keyMappingsBuilder);
            mapAllToValue(Keys.entity_allow_keys, new Keyed<>(Registry.ENTITY_TYPE, Configurator::getEntityAllowSet), keyMappingsBuilder);
            mapAllToValue(Keys.entity_deny_keys, new Keyed<>(Registry.ENTITY_TYPE, Configurator::getEntityDenySet), keyMappingsBuilder);

            keyMappings = keyMappingsBuilder.build();
        }

        private static <K, V> void mapAllToValue(Collection<K> keys, V value, ImmutableMap.Builder<K, V> builder) {
            for (K key : keys) builder.put(key, value);
        }
    }

    private record Keyed<T>(Registry<T> registry, Function<Configurator, Collection<T>> collectionFunction) {

        public void resolveIdString(Configurator configurator, String idString) {
            this.collectionFunction.apply(configurator).addAll(resolveIdString(idString, this.registry));
        }

        public static <T> Collection<T> resolveIdString(String idString, Registry<T> registry) {
            Collection<T> collection = new LinkedList<>();
            if (idString.startsWith("#")) {
                final Identifier id = new Identifier(idString.substring(1));
                try {
                    final List<T> values = new LinkedList<>();
                    registry.getTagOrEmpty(TagKey.of(registry.getKey(), id)).iterator().forEachRemaining(holder -> values.add(holder.value()));
                    collection.addAll(values);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                // individual id
                Identifier identifier = new Identifier(idString);
                T t = registry.get(identifier);
                // registry.get(-1) returns registry's default value
                if (t == registry.get(-1))
                    LOGGER.error("Item '" + idString + "' does not exist. ");
                else
                    collection.add(t);
            }
            return collection;
        }
    }
}
