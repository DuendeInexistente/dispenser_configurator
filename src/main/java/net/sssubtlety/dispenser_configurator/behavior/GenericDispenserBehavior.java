package net.sssubtlety.dispenser_configurator.behavior;

import com.google.common.collect.ImmutableList;
import net.minecraft.block.Block;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.sssubtlety.dispenser_configurator.DummyPlayer;
import net.sssubtlety.dispenser_configurator.behavior.delegate.DispenserBehaviorDelegate;
import net.sssubtlety.dispenser_configurator.behavior.delegate.DualListPredicated;
import net.sssubtlety.dispenser_configurator.behavior.predicate.DualSet;
import org.apache.commons.lang3.mutable.MutableObject;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class GenericDispenserBehavior extends FallibleItemDispenserBehavior {
    private static final DummyPlayer.Manager DUMMY_MANAGER = new DummyPlayer.Manager();

    protected final ImmutableList<DispenserBehaviorDelegate> delegates;
//    protected final boolean hasBlockBehavior;
//    protected final boolean hasEntityBehavior;

    protected final DualSet<Block> blockList;
    protected final DualSet<EntityType<?>> entityList;
    protected final Identifier id;
    protected final boolean exclusive;

    public GenericDispenserBehavior(List<DispenserBehaviorDelegate> delegates, DualSet<Block> blockPredicate, DualSet<EntityType<?>> entityPredicate, Identifier id, boolean exclusive) {
        this.exclusive = exclusive;

        List<DualListPredicated> dualListPredicatedDelegates = delegates.stream()
                .filter(delegate -> delegate instanceof DualListPredicated)
                .map(delegate -> (DualListPredicated)delegate)
                .collect(Collectors.toList());

        if (!blockPredicate.isEmpty()) {
            boolean usedBlockPredicate = false;
            for (DualListPredicated delegate : dualListPredicatedDelegates)
                usedBlockPredicate |= delegate.considerDualList(blockPredicate);

            if (!usedBlockPredicate) LOGGER.warn("Block" + getListNotUsedString(id));
        }

        if (!entityPredicate.isEmpty()) {
            boolean usedEntityPredicate = false;

            for (DualListPredicated delegate : dualListPredicatedDelegates)
                usedEntityPredicate |= delegate.considerDualList(entityPredicate);

            if (!usedEntityPredicate)
                LOGGER.warn("Entity" + getListNotUsedString(id));
        }
        for (DispenserBehaviorDelegate delegate : delegates) delegate.finishInitialization();

        this.delegates = ImmutableList.copyOf(delegates);
        this.blockList = blockPredicate;
        this.entityList = entityPredicate;
        this.id = id;
    }

    @NotNull
    private static String getListNotUsedString(Identifier id) {
        return " list in " + id + " was not used by any targets.";
    }

    public static <T> void fillIfEmpty(MutableObject<T> mutable, Supplier<T> filler) {
        if (mutable.getValue() == null)
            mutable.setValue(filler.get());
    }

    @Override
    public ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        if (pointer.getWorld().isClient()) return stack;

        this.setSuccess(false);
        MutableObject<BlockHitResult> mutableBlockHitResult = new MutableObject<>();
        MutableObject<List<LivingEntity>> mutableLivingEntityList = new MutableObject<>();

        DummyPlayer dummy = DUMMY_MANAGER.getDummy(pointer.getWorld());
        dummy.setWorld(pointer.getWorld());

        for (DispenserBehaviorDelegate delegate : delegates) {
            dummy.setStackInHand(Hand.MAIN_HAND, stack);
            final PlayerInventory dummyInventory = dummy.getInventory();
            int inventoryChanges = dummyInventory.getChangeCount();
            if (delegate.behaviorDelegation(pointer, pointer.getBlockState().get(DispenserBlock.FACING), dummy, mutableBlockHitResult, mutableLivingEntityList)) {
                stack = dummy.getStackInHand(Hand.MAIN_HAND);
                delegate.onSuccess(pointer);
                this.setSuccess(true);

                if (dummyInventory.getChangeCount() - inventoryChanges > 2)
                    // this should cover anything added to the dummy's inventory not in their main hand
                    //  such as when using more than one bottle on a beehive or cauldron you get a bottle ox <x> back,
                    //  in addition to keeping the stack of bottles (with count--)
                    DispenserBehaviorDelegate.tryInsertPlayerItems(dummy, pointer);

                break;
            }

            dummyInventory.clear();
        }

        DUMMY_MANAGER.releaseDummy(dummy);

        return stack;
    }
}
