package net.sssubtlety.dispenser_configurator.behavior;

import java.util.*;
import java.util.function.Function;

public class SortedList<T> extends ArrayList<T> {
    protected final Comparator<T> comparator;
    protected final Function<Object, T> caster;

    public SortedList(Comparator<T> comparator, Function<Object, T> caster) {
        this.comparator = comparator;
        this.caster = caster;
    }

    public SortedList(Comparator<T> comparator, Function<Object, T> caster, T t) {
        this(comparator, caster);
        super.add(t);
    }

    protected int find(T t) {
        return Collections.binarySearch(this, t, comparator);
    }

    @Deprecated
    @Override
    public T set(int index, T element) throws UnsupportedOperationException {
        throw unsupportedIndexOperation("setting");
    }

    @Override
    public boolean add(T t) {
        insert(Math.abs(find(t)), t);
        return true;
    }

    protected final void append(T element) {
        super.add(element);
    }

    @Deprecated
    @Override
    public void add(int index, T element) {
        throw unsupportedIndexOperation("adding");
    }

    protected final void insert(int index, T element) {
        super.add(index, element);
    }

    @Override
    public boolean remove(Object o) {
        T t = caster.apply(o);
        if (t == null) return false;
        else {
            final int index = find(t);
            if (index >= 0) {
                super.remove(index);
                return true;
            } else return false;
        }
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean changed = false;
        for (T t : c)
            changed |= add(t);

        return changed;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        throw unsupportedIndexOperation("adding");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean changed = false;
        for (Object o : c)
            changed |= remove(o);

        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean changed = false;
        Iterator<T> itr = this.iterator();
        while (itr.hasNext()) {
            T t = itr.next();
            if (!c.contains(t)) {
                itr.remove();
                changed = true;
            }
        }

        return changed;
    }

    @Deprecated
    @Override
    public void sort(Comparator<? super T> c) {
        super.sort(c);
    }

    @Override
    public boolean contains(Object o) {
        T t = caster.apply(o);
        return t != null && find(t) >= 0;
    }

    @Override
    public int indexOf(Object o) {
        T t = caster.apply(o);
        if (t == null) return -1;
        return find(t);
    }

    @Override
    public int lastIndexOf(Object o) {
        T t = caster.apply(o);
        if (t == null) return -1;
        int index = find(t);
        if (index < 0) return -1;
        final int size = this.size();
        while(index < size && comparator.compare(t,this.get(index)) == 0) index++;
        return index;
    }

    protected static UnsupportedOperationException unsupportedIndexOperation(String indexMethodDescription) {
        return new UnsupportedOperationException("SortedList does not support " + indexMethodDescription + " by index.");
    }
}
