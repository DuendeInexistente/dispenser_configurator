package net.sssubtlety.dispenser_configurator.behavior;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class PriorityBehaviorMultiMap {
    public static Builder builder() {
        return new Builder();
    }

    private ImmutableMap<Item, ImmutableList<GenericDispenserBehavior>> behaviors;

    protected PriorityBehaviorMultiMap(ImmutableMap<Item, ImmutableList<GenericDispenserBehavior>> behaviors) {
        this.behaviors = behaviors;
    }

    public PriorityBehaviorMultiMap() {
        this.behaviors = ImmutableMap.of();
    }

    public void reset(Builder builder) {
        this.behaviors = builder.toBehaviorMap();
    }

    public Collection<GenericDispenserBehavior> get(ItemStack stack) {
         return behaviors.get(stack.getItem());
    }

//    public void clear() {
//        behaviors = ImmutableMap.of();
//    }

    public static class Builder {
        private final Map<Item, PrioritizedConfigurators> configuratorMap;

        protected Builder() {
            this.configuratorMap = new HashMap<>();
        }

        public PriorityBehaviorMultiMap build() {
            return new PriorityBehaviorMultiMap(toBehaviorMap());
        }

        public ImmutableMap<Item, ImmutableList<GenericDispenserBehavior>> toBehaviorMap() {
            return configuratorMap.entrySet().stream().collect(ImmutableMap.toImmutableMap(
                Map.Entry::getKey,
                entry -> ImmutableList.copyOf(entry.getValue().stream().map(Configurator::resolveBehavior).toList())
            ));
        }

        public void createMappedBehavior(Configurator configurator) {
            for (Item item : configurator.getItems()) {
                final PrioritizedConfigurators currentConfigurators = configuratorMap.get(item);
                if (currentConfigurators == null)
                    configuratorMap.put(item, new PrioritizedConfigurators(configurator));
                else currentConfigurators.add(item, configurator);
            }
        }
    }
}
