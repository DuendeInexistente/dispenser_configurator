package net.sssubtlety.dispenser_configurator.behavior.predicate.universal;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;
import net.sssubtlety.dispenser_configurator.behavior.predicate.DualSet;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public class DualSetUniversalAcceptor<I> extends DualSet<I> {
    public static final DualSetUniversalAcceptor<Block> BLOCK_ACCEPTOR = new DualSetUniversalAcceptor<>();
    public static final DualSetUniversalAcceptor<EntityType<?>> ENTITY_TYPE_ACCEPTOR = new DualSetUniversalAcceptor<>();
//    public static final DualListUniversalAcceptor<BlockItem> BLOCK_ITEM_ACCEPTOR = new DualListUniversalAcceptor<>();

    private DualSetUniversalAcceptor() { }

    @Override public boolean test(I item) {
        return true;
    }

    @Override public boolean add(Object o) { return false; }
    @Override public boolean remove(Object o) { return false; }
    @Override public boolean addAll(@NotNull Collection<? extends I> c) { return false; }
    @Override public boolean removeAll(@NotNull Collection<?> c) { return false; }
    @Override public boolean retainAll(@NotNull Collection<?> c) { return false; }
    @Override public void clear() { }
}
