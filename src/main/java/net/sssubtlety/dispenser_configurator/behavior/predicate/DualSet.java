package net.sssubtlety.dispenser_configurator.behavior.predicate;

import com.google.common.collect.Streams;
import net.sssubtlety.dispenser_configurator.behavior.predicate.universal.AllowSetUniversalAcceptor;
import net.sssubtlety.dispenser_configurator.behavior.predicate.universal.DenySetUniversalAcceptor;
import net.sssubtlety.dispenser_configurator.behavior.predicate.universal.DualSetUniversalAcceptor;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.stream.Stream;

public class DualSet<T> implements CollectionPredicate<T> {
    public static final String ADD_NOT_SUPPORTED = "add is not supported. Use allow or deny instead.";
    public final DenySet<T> denySet;
    public final AllowSet<T> allowSet;

    public DualSet() {
        this.denySet = new DenySet<>();
        this.allowSet = new AllowSet<>();
    }

    public DualSet(Collection<T> denyList, Collection<T> allowList) {
        this.denySet = new DenySet<>(denyList);
        this.allowSet = new AllowSet<>(allowList);
    }

//    public static <T> DualSet<T> getDualSet(Collection<T> allowable, Collection<T> deniable, AllowSetUniversalAcceptor<T> allowAcceptor, DenySetUniversalAcceptor<T> denyAcceptor, DualSetUniversalAcceptor<T> dualAcceptor) {
//        final boolean emptyDeny = deniable.isEmpty();
//        final boolean emptyAllow = allowable.isEmpty();
//
//        if (emptyDeny && emptyAllow) return dualAcceptor;
//        else {
//            final DenySet<T> denySet = emptyDeny ? denyAcceptor : new DenySet<>(deniable);
//            final AllowSet<T> allowSet = emptyAllow ? allowAcceptor : new AllowSet<>(allowable);
//            return new DualSet<>(denySet, allowSet);
//        }
//    }

    @Override
    public int size() {
        return denySet.size() + allowSet.size();
    }

    @Override
    public boolean isEmpty() {
        return denySet.isEmpty() && allowSet.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return denySet.contains(o) || allowSet.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return getCombinedStream().iterator();
    }

    @Override
    public Object[] toArray() {
        return getCombinedStream().toArray();
    }

    @Override
    public <T_> T_[] toArray(T_ @NotNull [] a) {
        if (a.length <= size()) {
            Iterator<T> itr = iterator();
            for (int i = 0; i < a.length; i++) {
                a[i] = (T_) itr.next();
            }
        }
        return (T_[]) toArray() ;
    }

    @Override
    public boolean add(T t) throws UnsupportedOperationException {
        throw new UnsupportedOperationException(ADD_NOT_SUPPORTED);
    }

    public boolean allow(T t) {
        if (t == null) return false;
        return allowSet.add(t);
    }

    public boolean deny(T t) {
        return denySet.add(t);
    }

    @Override
    public boolean remove(Object o) {
        return denySet.remove(o) || allowSet.remove(o);
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> c) {
        Collection<T> combinedView = getCombinedView();
        return combinedView.containsAll(c);
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends T> c) throws UnsupportedOperationException {
        throw new UnsupportedOperationException(ADD_NOT_SUPPORTED);
    }

    public boolean allowAll(@NotNull Collection<? extends T> c) {
        return allowSet.addAll(c);
    }

    public boolean denyAll(@NotNull Collection<? extends T> c) {
        return denySet.addAll(c);
    }

    @Override
    public boolean removeAll(@NotNull Collection<?> c) {
        return denySet.removeAll(c) | allowSet.removeAll(c);
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> c) {
        return denySet.retainAll(c) | allowSet.retainAll(c);
    }

    @Override
    public void clear() {
        denySet.clear();
        allowSet.clear();
    }

    @Override
    public boolean test(T t) {
        return denySet.test(t) && allowSet.test(t);
    }

    private Collection<T> getCombinedView() {
        Collection<T> combinedView = new LinkedList<>(this.denySet);
        combinedView.addAll(this.allowSet);
        return combinedView;
    }

    @SuppressWarnings("UnstableApiUsage")
    private Stream<T> getCombinedStream() {
        return Streams.concat(allowSet.stream(), denySet.stream());
    }

    @Override
    public boolean intersects(CollectionPredicate<T> other) {
        for (T t : this.allowSet)
            if (this.denySet.test(t) && other.test(t)) return true;

        return false;
    }
}
