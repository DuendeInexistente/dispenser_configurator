package net.sssubtlety.dispenser_configurator.behavior.predicate;

import java.util.Collection;

public class AllowSet<T> extends LinkedHashSetPredicate<T> {
    public AllowSet() { }

    public AllowSet(Collection<T> collection) {
        super(collection);
    }

    @Override public boolean test(T item) {
        return this.contains(item);
    }

    @Override
    public final boolean intersects(CollectionPredicate<T> other) {
        for (T t : this) if (other.test(t)) return true;

        return false;
    }
}
