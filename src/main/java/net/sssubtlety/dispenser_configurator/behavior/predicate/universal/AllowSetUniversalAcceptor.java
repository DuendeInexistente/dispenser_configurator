package net.sssubtlety.dispenser_configurator.behavior.predicate.universal;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;
import net.sssubtlety.dispenser_configurator.behavior.predicate.AllowSet;

import java.util.Collection;

public class AllowSetUniversalAcceptor<I> extends AllowSet<I> {
    public static final AllowSetUniversalAcceptor<Block> BLOCK_ACCEPTOR = new AllowSetUniversalAcceptor<>();
    public static final AllowSetUniversalAcceptor<EntityType<?>> ENTITY_ACCEPTOR = new AllowSetUniversalAcceptor<>();

    private AllowSetUniversalAcceptor() { }

    @Override public boolean test(I item) {
        return true;
    }

    @Override public boolean add(Object o) { return false; }
    @Override public boolean remove(Object o) { return false; }
    @Override public boolean addAll(Collection<? extends I> c) { return false; }
    @Override public boolean removeAll(Collection<?> c) { return false; }
    @Override public boolean retainAll(Collection<?> c) { return false; }
    @Override public void clear() { }
}
