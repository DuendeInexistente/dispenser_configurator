package net.sssubtlety.dispenser_configurator.behavior.predicate.universal;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;
import net.sssubtlety.dispenser_configurator.behavior.predicate.DenySet;

import java.util.Collection;

public class DenySetUniversalAcceptor<I> extends DenySet<I> {
    public static final DenySetUniversalAcceptor<Block> BLOCK_ACCEPTOR = new DenySetUniversalAcceptor<>();
    public static final DenySetUniversalAcceptor<EntityType<?>> ENTITY_ACCEPTOR = new DenySetUniversalAcceptor<>();

    private DenySetUniversalAcceptor() { }

    @Override public boolean test(I t) {
        return true;
    }

    @Override public boolean add(Object o) { return false; }
    @Override public boolean remove(Object o) { return false; }
    @Override public boolean addAll(Collection<? extends I> c) { return false; }
    @Override public boolean removeAll(Collection<?> c) { return false; }
    @Override public boolean retainAll(Collection<?> c) { return false; }
    @Override public void clear() { }
}
