package net.sssubtlety.dispenser_configurator.behavior.predicate;

import java.util.*;
import java.util.function.Predicate;

/**
* A collection that is also a predicate
* */
public interface CollectionPredicate<T> extends Collection<T>, Predicate<T> {
    boolean intersects(CollectionPredicate<T> other);
}
