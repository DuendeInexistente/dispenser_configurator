package net.sssubtlety.dispenser_configurator.behavior.predicate;

import java.util.Collection;
import java.util.LinkedHashSet;

public abstract class LinkedHashSetPredicate<T> extends LinkedHashSet<T> implements CollectionPredicate<T>, SimplePredicate<T> {
    public LinkedHashSetPredicate() { }

    public LinkedHashSetPredicate(Collection<T> collection) {
        super(collection);
    }
}
