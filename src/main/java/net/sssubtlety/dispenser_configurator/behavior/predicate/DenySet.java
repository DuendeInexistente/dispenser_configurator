package net.sssubtlety.dispenser_configurator.behavior.predicate;

import java.util.Collection;

public class DenySet<T> extends LinkedHashSetPredicate<T> {
    public DenySet() { }

    public DenySet(Collection<T> collection) {
        super(collection);
    }

    @Override
    public boolean test(T t) {
        return !this.contains(t);
    }

    @Override
    public boolean intersects(CollectionPredicate<T> other) {
        // A denyList's set of potentially acceptable Ts is unbounded, so we must assume it intersects all other non-bounded CollectionPredicates
        if (other instanceof AllowSet<T> allowSet)
            // whitelists are bounded
            return !this.containsAll(allowSet);

        return true;
    }
}
