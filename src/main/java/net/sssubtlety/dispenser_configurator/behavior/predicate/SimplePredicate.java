package net.sssubtlety.dispenser_configurator.behavior.predicate;

import org.jetbrains.annotations.NotNull;

import java.util.function.Predicate;

public interface SimplePredicate<T> extends Predicate<T> {
    @Override
    default Predicate<T> and(@NotNull Predicate<? super T> other) {
        return (item) -> this.test(item) && other.test(item);
    }

    @Override
    default Predicate<T> negate() {
        return (item) -> !this.test(item);
    }

    @Override
    default Predicate<T> or(@NotNull Predicate<? super T> other) {
        return (item) -> this.test(item) || other.test(item);
    }
}
