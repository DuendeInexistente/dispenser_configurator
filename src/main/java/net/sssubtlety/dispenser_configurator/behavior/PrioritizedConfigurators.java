package net.sssubtlety.dispenser_configurator.behavior;

import net.minecraft.item.Item;
import net.minecraft.util.registry.Registry;

import static net.sssubtlety.dispenser_configurator.DispenserConfigurator.LOGGER;

public class PrioritizedConfigurators extends SortedList<Configurator> {
    protected static Configurator cast(Object o) {
        return o instanceof Configurator configurator ? configurator : null;
    }

    protected static void logConflictError(Item item, Configurator current, Configurator conflict) {
        LOGGER.error("Conflicting exclusive configurators found for item: " + Registry.ITEM.getId(item) +
                ". Configurators: '" + current.id + "', '" + conflict.id + "'.\n" +
                "Skipping " + "'" + conflict.id + "'.");
    }

    public PrioritizedConfigurators() {
        super(Configurator::comparePriority, PrioritizedConfigurators::cast);
    }

    public PrioritizedConfigurators(Configurator configurator) {
        this();
        this.append(configurator);
    }

    public void add(Item item, Configurator configurator) {
        int firstIndex = find(configurator);
        if (firstIndex >= 0) { // found configurator with == priority
            Configurator equalPriorityConfigurator = get(firstIndex);
            if (configurator.intersects(equalPriorityConfigurator) && equalPriorityConfigurator.exclusive) {
                if (configurator.exclusive) logConflictError(item, equalPriorityConfigurator, configurator);
                return;
            }
        } else firstIndex = -firstIndex - 1;

        if (firstIndex < size()) insert(firstIndex, configurator);
        else append(configurator);
    }
}
