package net.sssubtlety.dispenser_configurator.behavior;

import net.minecraft.block.dispenser.ItemDispenserBehavior;
import net.minecraft.block.entity.DispenserBlockEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPointer;

public interface PotentialPlayerOutputDispenserInterface {
    ItemDispenserBehavior itemDispenserBehavior = new ItemDispenserBehavior();

    static void tryInsertPlayerItems(ServerPlayerEntity player, BlockPointer pointer) {
        DispenserBlockEntity dispenserBlockEntity = pointer.getBlockEntity();
        if (dispenserBlockEntity != null) {
            for (ItemStack stack : player.getInventory().main) {
                if (!stack.isEmpty() &&
                        //skipping main hand: it will be handled separately
                        stack != player.getStackInHand(Hand.MAIN_HAND) &&
                        !insertIntoDispenser(dispenserBlockEntity, stack)) {
                    itemDispenserBehavior.dispense(pointer, stack);
                }
            }
        }
    }

//    static ItemStack putBackPlayerItem(ItemStack stack, DummyPlayer dummyPlayer) {
//        ItemStack mainHandItem = dummyPlayer.getStackInHand(Hand.MAIN_HAND);
//        if (stack.isEmpty()) {
//            return mainHandItem;
//        } else if(!mainHandItem.isEmpty()){
//            stack.increment(1);
//        }
//        return stack;
//    }

    static boolean insertIntoDispenser(DispenserBlockEntity dispenserBlockEntity, ItemStack stack) {
        int invSize = dispenserBlockEntity.size();
        int firstEmptySlot = invSize;
        ItemStack slotStack;
        boolean stackStackable = stack.isStackable();

        for (int slot = 0; slot < invSize; slot++) {
            slotStack = dispenserBlockEntity.getStack(slot);

            if(stackStackable) {
                if (slotStack.isEmpty() && slot < firstEmptySlot)
                    firstEmptySlot = slot;
                else {
                    if (stack.isItemEqual(slotStack)) {
                        int transferring = Math.min(slotStack.getMaxCount() - slotStack.getCount(), stack.getCount());
                        slotStack.increment(transferring);
                        if (transferring > 0) {
                            stack.decrement(transferring);
                            if (stack.isEmpty()) { break; }
                        }
                    }
                }
            } else if (slotStack.isEmpty()) {
                firstEmptySlot = slot;
                break;
            }
        }

        if(stack.isEmpty()) {
            return true;
        } else if (firstEmptySlot < invSize) {
            dispenserBlockEntity.setStack(firstEmptySlot, stack);
            return true;
        }

        return false;
    }
}
