package net.sssubtlety.dispenser_configurator.behavior;

import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Streams;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.registry.Registry;

import java.util.Collection;
import java.util.List;

import static net.sssubtlety.dispenser_configurator.DispenserConfigurator.LOGGER;

public class GenericBehaviorMultiMap {
    private final Multimap<Item, GenericDispenserBehavior> highPriorityBehaviors;
    private final Multimap<Item, GenericDispenserBehavior> lowPriorityBehaviors;

    protected static void logConflictError(Item item, GenericDispenserBehavior behavior1, GenericDispenserBehavior behavior2) {
        LOGGER.error("Conflicting exclusive configurators found for item: " + Registry.ITEM.getId(item) + ". Configurators: " + behavior1.id + ", " + behavior2.id);
    }

    public GenericBehaviorMultiMap() {
        highPriorityBehaviors = LinkedHashMultimap.create();
        lowPriorityBehaviors = LinkedHashMultimap.create();
    }

//    public void mapBehavior(Collection<Item> items, GenericDispenserBehavior behavior) {
//        for (Item item : items) {
//            for (GenericDispenserBehavior oldBehavior :
//                    // aggregated behaviors
//                    getAggregatedBehaviors(item)) {
//                if (behavior.exclusive || oldBehavior.exclusive) {
//                    if (behavior.intersects(oldBehavior)) {
//                        if (oldBehavior.exclusive) {
//                            if (behavior.exclusive) logConflictError(item, behavior, oldBehavior);
//                            return;
//                        }
//                    }
//                }
//            }
//
//            (behavior.isLowPriority() ? lowPriorityBehaviors : highPriorityBehaviors).put(item, behavior);
//        }
//    }

    private List<GenericDispenserBehavior> getAggregatedBehaviors(Item item) {
        return Streams.concat(highPriorityBehaviors.get(item).stream(), lowPriorityBehaviors.get(item).stream()).toList();
    }

    public Collection<GenericDispenserBehavior> get(ItemStack stack) {
         return getAggregatedBehaviors(stack.getItem());
    }

    public void clear() {
        highPriorityBehaviors.clear();
        lowPriorityBehaviors.clear();
    }
}
