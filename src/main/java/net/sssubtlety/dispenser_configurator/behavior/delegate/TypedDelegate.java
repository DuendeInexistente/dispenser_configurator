package net.sssubtlety.dispenser_configurator.behavior.delegate;

import net.sssubtlety.dispenser_configurator.behavior.predicate.CollectionPredicate;
import net.sssubtlety.dispenser_configurator.behavior.predicate.DualSet;
import org.jetbrains.annotations.Nullable;

import java.util.function.Function;
import java.util.function.Predicate;

import static net.sssubtlety.dispenser_configurator.DispenserConfigurator.LOGGER;

public abstract class TypedDelegate<T> extends DualListedDelegate {
    protected DualSet<T> dualSet;

    protected static  <T_, T> @Nullable DualSet<T> tryBuildList(DualSet<? extends T_> source, Function<Object, T> caster) {
        DualSet<T> tList = new DualSet<>();

        if (
            tryFillCollectionPredicate(source.allowSet, caster, tList::allow, true) &&
            tryFillCollectionPredicate(source.denySet, caster, tList::deny, false)
        ) return tList;
        else return null;
    }

    protected static  <T_, T> boolean tryFillCollectionPredicate(CollectionPredicate<? extends T_> source, Function<Object, T> caster, Predicate<T> filler, boolean first) {
        for (T_ t_ : source) {
            T t = caster.apply(t_);

            if (filler.test(t)) first = false;
            else {
                if (!first) LOGGER.error("Found inconsistent types in considerCollection.");
                return false;
            }
        }
        return !first;
    }

    protected static boolean failReset() {
        LOGGER.error("Trying to re-set CollectionPredicate but it must only be set once.");
        return false;
    }

    @Override
    protected final boolean allows(Object candidate) {
        T tCandidate = tryCast(candidate);
        if (tCandidate != null) return dualSet.test(tCandidate);
        else return false;
    }

    @Override
    public final <T_> boolean considerDualList(DualSet<? extends T_> dualSet) {
        final DualSet<T> potentialList = tryBuildList(dualSet, this::tryCast);
        if (potentialList != null) {
            if (this.dualSet == null) {
                this.dualSet = potentialList;
                return true;
            } else return failReset();
        }

        return false;
    }

    protected abstract @Nullable T tryCast(Object obj);
}
