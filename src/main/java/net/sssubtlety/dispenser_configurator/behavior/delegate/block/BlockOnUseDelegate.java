package net.sssubtlety.dispenser_configurator.behavior.delegate.block;

import net.minecraft.block.BlockState;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.sssubtlety.dispenser_configurator.DummyPlayer;
import net.sssubtlety.dispenser_configurator.behavior.PotentialPlayerOutputDispenserInterface;

public class BlockOnUseDelegate extends BlockDelegate {

    public static final String DELEGATE_NAME = "BLOCK_USE";

    static {
        putDelegate(DELEGATE_NAME, BlockOnUseDelegate::new);
    }

    public static void init() { }

    @Override
    public String getName() {
        return DELEGATE_NAME;
    }

    @Override
    protected boolean behaviorDelegationImplementation(DummyPlayer dummy, BlockPointer dispenserPointer, BlockHitResult blockHitResult, BlockState facingBlockState) {
        final ActionResult result = facingBlockState.onUse(dispenserPointer.getWorld(), dummy, Hand.MAIN_HAND, blockHitResult);
        if (result.isAccepted()) {
            PotentialPlayerOutputDispenserInterface.tryInsertPlayerItems(dummy, dispenserPointer);
            syncSuccessEffect(dispenserPointer);
            return true;
        }

        return false;
    }
}
