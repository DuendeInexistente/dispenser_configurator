package net.sssubtlety.dispenser_configurator.behavior.delegate;

import net.sssubtlety.dispenser_configurator.behavior.predicate.DualSet;
import org.jetbrains.annotations.Nullable;

public abstract class DoubleTypedDelegate<A,B> extends DualListedDelegate {
    protected DualSet<A> aDualSet;
    protected DualSet<B> bDualSet;

    @Override
    protected final boolean allows(Object candidate) {
        A aCandidate = tryCastA(candidate);
        if (aCandidate != null) return aDualSet.test(aCandidate);

        B bCandidate = tryCastB(candidate);
        if (bCandidate != null) return bDualSet.test(bCandidate);

        return false;
    }

    @Override
    public final <T_> boolean considerDualList(DualSet<? extends T_> dualSet) {
        final DualSet<A> potentialAList = TypedDelegate.tryBuildList(dualSet, this::tryCastA);
        if (potentialAList != null) {
            if (aDualSet == null) {
                aDualSet = potentialAList;
                return true;
            } else return TypedDelegate.failReset();
        }

        final DualSet<B> potentialBList = TypedDelegate.tryBuildList(dualSet, this::tryCastB);
        if (potentialBList != null) {
            if (bDualSet == null) {
                bDualSet = potentialBList;
                return true;
            } else return TypedDelegate.failReset();
        }

        return false;
    }

    //    @Override
//    public final <T_> boolean considerCollection(Collection<? extends T_> collection) {
//        Iterator<? extends T_> itr = collection.iterator();
//        if (itr.hasNext()) {
//            T_ first = itr.next();
//            // TODO: consider alternatives to this sus sampling
//            A aSample = tryCastA(first);
//            if (aSample != null) {
//                if (this.aList != null) LOGGER.error("Trying to reset predicatedList but it's only meant to be set once.");
//                else this.aList = collection.stream().map(this::tryCastA).collect(Collectors.toCollection(DualList::new));
//                return true;
//            }
//
//            B bSample = tryCastB(first);
//            if (bSample != null) {
//                if (this.bList != null) LOGGER.error("Trying to reset predicatedList but it's only meant to be set once.");
//                else this.bList = collection.stream().map(this::tryCastB).collect(Collectors.toCollection(DualList::new));
//                return true;
//            }
//        }
//
//        return false;
//    }

    protected abstract @Nullable A tryCastA(Object obj);
    protected abstract @Nullable B tryCastB(Object obj);
}
