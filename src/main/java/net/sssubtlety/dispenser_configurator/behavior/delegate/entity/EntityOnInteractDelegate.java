package net.sssubtlety.dispenser_configurator.behavior.delegate.entity;

import net.minecraft.entity.LivingEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPointer;
import net.sssubtlety.dispenser_configurator.DummyPlayer;
import net.sssubtlety.dispenser_configurator.behavior.PotentialPlayerOutputDispenserInterface;

public class EntityOnInteractDelegate extends EntityTypeDelegate {

    public static final String DELEGATE_NAME = "ENTITY_ON_INTERACT";

    static { putDelegate(DELEGATE_NAME, EntityOnInteractDelegate::new); }

    public static void init() { }

    @Override
    public String getName() {
        return DELEGATE_NAME;
    }

    @Override
    protected boolean behaviorDelegationImplementation(DummyPlayer dummy, BlockPointer dispenserPointer, LivingEntity livingEntity) {
            ActionResult result = livingEntity.interact(dummy, Hand.MAIN_HAND);
            if (result.isAccepted()) {
                PotentialPlayerOutputDispenserInterface.tryInsertPlayerItems(dummy, dispenserPointer);
                return true;
            }
        return false;
    }
}
