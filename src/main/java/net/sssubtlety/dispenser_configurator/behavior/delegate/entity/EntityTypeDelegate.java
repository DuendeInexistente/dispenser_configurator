package net.sssubtlety.dispenser_configurator.behavior.delegate.entity;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Direction;
import net.sssubtlety.dispenser_configurator.DummyPlayer;
import net.sssubtlety.dispenser_configurator.behavior.delegate.TypedDelegate;
import net.sssubtlety.dispenser_configurator.behavior.predicate.universal.DualSetUniversalAcceptor;
import org.apache.commons.lang3.mutable.MutableObject;

import java.util.List;

import static net.sssubtlety.dispenser_configurator.behavior.GenericDispenserBehavior.fillIfEmpty;

public abstract class EntityTypeDelegate extends TypedDelegate<EntityType<?>> {
    @Override
    public final boolean behaviorDelegation(BlockPointer dispenserPointer, Direction facing, DummyPlayer dummy, MutableObject<BlockHitResult> mutableBlockHitResult, MutableObject<List<LivingEntity>> mutableLivingEntityList) {
        fillIfEmpty(mutableLivingEntityList, () -> getPotentialEntities(dispenserPointer, facing));

        for (LivingEntity livingEntity : mutableLivingEntityList.getValue())
            if (this.allows(livingEntity.getType()) &&
                    behaviorDelegationImplementation(dummy, dispenserPointer, livingEntity))
                return true;

        return false;
    }

    protected abstract boolean behaviorDelegationImplementation(DummyPlayer dummy, BlockPointer dispenserPointer, LivingEntity livingEntity);

    public static List<LivingEntity> getPotentialEntities (BlockPointer dispenserPointer, Direction facing) {
        return dispenserPointer.getWorld().getNonSpectatingEntities(LivingEntity.class, new Box(dispenserPointer.getPos().offset(facing)));
    }

    @Override
    protected EntityType<?> tryCast(Object obj) {
        return obj instanceof EntityType<?> entityType ? entityType : null;
    }

    @Override
    public final void assignMissingLists() {
        if (dualSet == null) dualSet = DualSetUniversalAcceptor.ENTITY_TYPE_ACCEPTOR;
    }
}
