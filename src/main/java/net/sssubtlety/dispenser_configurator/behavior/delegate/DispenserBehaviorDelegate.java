package net.sssubtlety.dispenser_configurator.behavior.delegate;

import net.minecraft.block.dispenser.BlockPlacementDispenserBehavior;
import net.minecraft.block.dispenser.ItemDispenserBehavior;
import net.minecraft.block.entity.DispenserBlockEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;
import net.sssubtlety.dispenser_configurator.DummyPlayer;
import org.apache.commons.lang3.mutable.MutableObject;

import java.util.*;
import java.util.function.Supplier;

public abstract class DispenserBehaviorDelegate {
    public static final Map<String, Supplier<DispenserBehaviorDelegate>> DELEGATE_FACTORY_MAP = new LinkedHashMap<>();

    protected static final ItemDispenserBehavior DROP_BEHAVIOR = new ItemDispenserBehavior();
    protected static final BlockPlacementDispenserBehavior PLACE_BEHAVIOR = new BlockPlacementDispenserBehavior();

    public static final int DISPENSER_SUCCESS_ID = 1000;
    public static final int DISPENSER_FAILURE_ID = 1001;

    protected static void putDelegate(String delegateName, Supplier<DispenserBehaviorDelegate> delegateSupplier) {
        DELEGATE_FACTORY_MAP.put(delegateName, delegateSupplier);
    }

    protected static BlockPos getFacingPos(BlockPointer blockPointer, Direction facing) {
        return blockPointer.getPos().offset(facing);
    }

    public static void syncSuccessEffect(BlockPointer pointer) {
        syncSuccessEffect(pointer.getWorld(), pointer.getPos());
    }

    public static void syncSuccessEffect(World world, BlockPos pos) {
        world.syncWorldEvent(DISPENSER_SUCCESS_ID, pos, 0);
    }

    public static void syncFailEffect(BlockPointer pointer) {
        syncFailEffect(pointer.getWorld(), pointer.getPos());
    }

    public static void syncFailEffect(World world, BlockPos pos) {
        world.syncWorldEvent(DISPENSER_FAILURE_ID, pos, 0);
        world.emitGameEvent(null, GameEvent.DISPENSE_FAIL, pos);
    }

    public abstract boolean behaviorDelegation(BlockPointer dispenserPointer, Direction facing, DummyPlayer dummy, MutableObject<BlockHitResult> mutableBlockHitResult, MutableObject<List<LivingEntity>> mutableLivingEntityList);

    protected abstract boolean allows(Object candidate);

    public abstract void finishInitialization();

    public boolean isExclusive() {
        return false;
    }

    public abstract String getName();

    public static void tryInsertPlayerItems(ServerPlayerEntity player, BlockPointer pointer) {
        DispenserBlockEntity dispenserBlockEntity = pointer.getBlockEntity();
        if (dispenserBlockEntity != null) {
            for (ItemStack stack : player.getInventory().main) {
                if (!stack.isEmpty() &&
                    //skipping main hand: it will be handled separately
                    stack != player.getMainHandStack() &&
                    !insertIntoDispenser(dispenserBlockEntity, stack)
                )
                    // eject items that can't fit into dispenser
                    DROP_BEHAVIOR.dispense(pointer, stack);
            }
        }
    }

    static boolean insertIntoDispenser(DispenserBlockEntity dispenserBlockEntity, ItemStack stack) {
        int invSize = dispenserBlockEntity.size();
        int firstEmptySlot = invSize;
        ItemStack slotStack;
        boolean stackStackable = stack.isStackable();

        for (int slot = 0; slot < invSize; slot++) {
            slotStack = dispenserBlockEntity.getStack(slot);

            if(stackStackable) {
                if (slotStack.isEmpty() && slot < firstEmptySlot)
                    firstEmptySlot = slot;
                else {
                    if (stack.isItemEqual(slotStack)) {
                        int transferring = Math.min(slotStack.getMaxCount() - slotStack.getCount(), stack.getCount());
                        slotStack.increment(transferring);
                        if (transferring > 0) {
                            stack.decrement(transferring);
                            if (stack.isEmpty()) { break; }
                        }
                    }
                }
            } else if (slotStack.isEmpty()) {
                firstEmptySlot = slot;
                break;
            }
        }

        if(stack.isEmpty()) {
            return true;
        } else if (firstEmptySlot < invSize) {
            dispenserBlockEntity.setStack(firstEmptySlot, stack);
            return true;
        }

        return false;
    }

    public void onSuccess(BlockPointer pointer) {
        syncSuccessEffect(pointer);
    }
}
