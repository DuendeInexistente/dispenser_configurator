package net.sssubtlety.dispenser_configurator.behavior.delegate.entity;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPointer;
import net.sssubtlety.dispenser_configurator.DummyPlayer;
import net.sssubtlety.dispenser_configurator.behavior.PotentialPlayerOutputDispenserInterface;

public class ItemUseOnEntityDelegate extends EntityTypeDelegate {

    public static final String DELEGATE_NAME = "USE_ON_ENTITY";

    static { putDelegate(DELEGATE_NAME, ItemUseOnEntityDelegate::new); }

    public ItemUseOnEntityDelegate() {
        super();
    }

    public static void init() { }

    @Override
    public String getName() {
        return DELEGATE_NAME;
    }

    @Override
    protected boolean behaviorDelegationImplementation(DummyPlayer dummy, BlockPointer dispenserPointer, LivingEntity livingEntity) {
        ItemStack stack = dummy.getMainHandStack();
        final ActionResult result = stack.getItem().useOnEntity(stack, dummy, livingEntity, Hand.MAIN_HAND);
        if (result.isAccepted()) {
            PotentialPlayerOutputDispenserInterface.tryInsertPlayerItems(dummy, dispenserPointer);
            return true;
        }
        return false;
    }
}
