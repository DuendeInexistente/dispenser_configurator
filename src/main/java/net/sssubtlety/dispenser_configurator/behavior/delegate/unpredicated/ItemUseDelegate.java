package net.sssubtlety.dispenser_configurator.behavior.delegate.unpredicated;

import net.minecraft.command.argument.EntityAnchorArgumentType;
import net.minecraft.entity.EntityPose;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.sssubtlety.dispenser_configurator.DummyPlayer;
import net.sssubtlety.dispenser_configurator.behavior.PotentialPlayerOutputDispenserInterface;
import org.apache.commons.lang3.mutable.MutableObject;

import java.util.List;

public class ItemUseDelegate extends UnPredicatedDelegate {

    public static final String DELEGATE_NAME = "ITEM_USE";

    static { putDelegate(DELEGATE_NAME, ItemUseDelegate::new); }

    public static void init() { }

    @Override
    public String getName() {
        return DELEGATE_NAME;
    }

    @Override
    public boolean behaviorDelegation(BlockPointer dispenserPointer, Direction facing, DummyPlayer dummy, MutableObject<BlockHitResult> mutableBlockHitResult, MutableObject<List<LivingEntity>> mutableLivingEntityList) {
        BlockPos dispenserBlockPos = dispenserPointer.getPos();
        // offset to center of block
        Vec3d dispenserPos = new Vec3d(dispenserBlockPos.getX() + 0.5, dispenserBlockPos.getY() + 0.5, dispenserBlockPos.getZ() + 0.5);

        Vec3d dummyEyesDest = new Vec3d (
                dispenserPos.getX(),
                dispenserPos.getY(),
                dispenserPos.getZ()
        );

        // offset
        switch (facing) {
            case NORTH  /*-Z*/: dummyEyesDest = dummyEyesDest.add(0, 0, -0.75); break;
            case SOUTH  /* Z*/: dummyEyesDest = dummyEyesDest.add(0, 0,  0.75); break;
            case EAST   /* X*/: dummyEyesDest = dummyEyesDest.add( 0.75, 0, 0); break;
            case WEST   /*-X*/: dummyEyesDest = dummyEyesDest.add(-0.75, 0, 0); break;
            case UP     /* Y*/: dummyEyesDest = dummyEyesDest.add(0,  0.75, 0); break;
            case DOWN   /*-Y*/: dummyEyesDest = dummyEyesDest.add(0, -0.75, 0); break;
        }

        Vec3d dummyFootDest = dummyEyesDest.add(0, -dummy.getActiveEyeHeight(EntityPose.STANDING, PlayerEntity.STANDING_DIMENSIONS), 0);


//        BlockPos posDown = dispenserPointer.getBlockPos().down();
        // move it to dimension of dispenser block
        dummy.setWorld(dispenserPointer.getWorld());
        // position head where dispenser is facing by putting feet 1 block down from where it's facing
        dummy.teleport(dummyFootDest.getX(), dummyFootDest.getY(), dummyFootDest.getZ());//facingPosDown.getX(), facingPosDown.getY(), facingPosDown.getZ());
        // look at dispenser
        dummy.lookAt(EntityAnchorArgumentType.EntityAnchor.EYES, dispenserPos);

//        HitResult hitResult = callRayTrace(dummy.world, dummy, /*this.fluid == Fluids.EMPTY ? RayTraceContext.FluidHandling.SOURCE_ONLY : */RayTraceContext.FluidHandling.NONE);
//
//        if (hitResult.getType() == HitResult.Type.BLOCK) {
//            BlockPos facingPosAway = facingPos.offset(facing);
//            Vec3d hitResultPos3d = hitResult.getPos();
//            BlockPos hitResultBlockPos = new BlockPos(hitResultPos3d);
//            if (facingPosAway.equals(hitResultBlockPos))
//                return false;
//
//        }

//        dummy.addStatusEffect(new StatusEffectInstance(StatusEffects.GLOWING, 5));
//
//        ((ServerWorld)dummy.world).spawnParticles(ParticleTypes.BARRIER, dummyEyesDest.x, dummyEyesDest.y, dummyEyesDest.z, 1, 0, 0, 0, 0);
//
//        ((ServerWorld)dummy.world).spawnParticles(ParticleTypes.BARRIER, dummyFootDest.x, dummyFootDest.y, dummyFootDest.z, 1, 0, 0, 0, 0);

        TypedActionResult<ItemStack> typedResult = dummy.getMainHandStack().use(dispenserPointer.getWorld(), dummy, Hand.MAIN_HAND);
        //dummy.getMainHandStack()
//                dummy.getStackInHand(Hand.MAIN_HAND).use(dispenserPointer.getWorld(), dummy, Hand.MAIN_HAND);
        ActionResult result = typedResult.getResult();
        if (result.isAccepted()) {
            dummy.setStackInHand(Hand.MAIN_HAND, typedResult.getValue());
            PotentialPlayerOutputDispenserInterface.tryInsertPlayerItems(dummy, dispenserPointer);
            return true;
        }

        return false;
    }
}
