package net.sssubtlety.dispenser_configurator.behavior.delegate;

import net.sssubtlety.dispenser_configurator.behavior.predicate.DualSet;

public interface DualListPredicated {
    void assignMissingLists();

    // returns true iff the collection will be used for predication (in allows)
    <T> boolean considerDualList(DualSet<? extends T> collection);
}
