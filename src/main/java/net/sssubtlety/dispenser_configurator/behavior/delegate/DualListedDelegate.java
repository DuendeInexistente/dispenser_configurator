package net.sssubtlety.dispenser_configurator.behavior.delegate;

public abstract class DualListedDelegate extends DispenserBehaviorDelegate implements DualListPredicated {
    @Override
    public void finishInitialization() {
        assignMissingLists();
    }
}
