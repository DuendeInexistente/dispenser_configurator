package net.sssubtlety.dispenser_configurator.behavior.delegate.unpredicated;

import net.sssubtlety.dispenser_configurator.behavior.delegate.DispenserBehaviorDelegate;

public abstract class UnPredicatedDelegate extends DispenserBehaviorDelegate {
    @Override
    public boolean allows(Object candidate) {
        return true;
    }

    @Override
    public void finishInitialization() { }
}
