package net.sssubtlety.dispenser_configurator.behavior.delegate.unpredicated;

import net.minecraft.entity.LivingEntity;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.Direction;
import net.sssubtlety.dispenser_configurator.DummyPlayer;
import org.apache.commons.lang3.mutable.MutableObject;

import java.util.List;

public class PlaceBlockDelegate extends UnPredicatedDelegate {
    public static final String DELEGATE_NAME = "PLACE_BLOCK";

    static { putDelegate(DELEGATE_NAME, PlaceBlockDelegate::new); }

    public static void init() { }

    @Override
    public String getName() {
        return DELEGATE_NAME;
    }

    @Override
    public void onSuccess(BlockPointer pointer) {
        // PLACE_BEHAVIOR makes its own sound, so prevent duplicate here
    }

    @Override
    public boolean behaviorDelegation(BlockPointer dispenserPointer, Direction facing, DummyPlayer dummy, MutableObject<BlockHitResult> mutableBlockHitResult, MutableObject<List<LivingEntity>> mutableLivingEntityList) {
        PLACE_BEHAVIOR.dispense(dispenserPointer, dummy.getMainHandStack());
        return PLACE_BEHAVIOR.isSuccess();
    }
}
