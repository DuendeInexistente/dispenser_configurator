package net.sssubtlety.dispenser_configurator.behavior.delegate.block_and_entity;

import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPointer;
import net.sssubtlety.dispenser_configurator.DummyPlayer;

public class FailDelegate extends BlockAndEntityDelegate {
    public static final String DELEGATE_NAME = "FAIL";

    static { putDelegate(DELEGATE_NAME, FailDelegate::new); }

    public static void init() { }

    @Override
    public String getName() {
        return DELEGATE_NAME;
    }

    @Override
    public boolean isExclusive() {
        return true;
    }

    @Override
    public void onSuccess(BlockPointer pointer) {
        syncFailEffect(pointer);
    }

    protected boolean blockDelegationImplementation(DummyPlayer dummy, BlockPointer dispenserPointer, BlockState facingBlockState) {
        return true;
    }

    protected boolean entityDelegationImplementation(DummyPlayer dummy, BlockPointer dispenserPointer, LivingEntity livingEntity) {
        return true;
    }
}
