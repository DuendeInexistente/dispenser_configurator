package net.sssubtlety.dispenser_configurator.behavior.delegate.block_and_entity;

import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPointer;
import net.sssubtlety.dispenser_configurator.DummyPlayer;

public class DropDelegate extends BlockAndEntityDelegate {
    public static final String DELEGATE_NAME = "DROP";

    static { putDelegate(DELEGATE_NAME, DropDelegate::new); }

    public static void init() { }

    @Override
    public String getName() {
        return DELEGATE_NAME;
    }

    @Override
    public boolean isExclusive() {
        return true;
    }

    @Override
    public void onSuccess(BlockPointer pointer) {
        // DROP_BEHAVIOR plays a sound, so prevent duplicate here
    }

    protected boolean blockDelegationImplementation(DummyPlayer dummy, BlockPointer dispenserPointer, BlockState facingBlockState) {
        DROP_BEHAVIOR.dispense(dispenserPointer, dummy.getMainHandStack());
        return true;
    }

    protected boolean entityDelegationImplementation(DummyPlayer dummy, BlockPointer dispenserPointer, LivingEntity livingEntity) {
        DROP_BEHAVIOR.dispense(dispenserPointer, dummy.getMainHandStack());
        return true;
    }

}
