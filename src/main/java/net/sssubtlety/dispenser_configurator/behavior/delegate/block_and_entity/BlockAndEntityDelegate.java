package net.sssubtlety.dispenser_configurator.behavior.delegate.block_and_entity;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.sssubtlety.dispenser_configurator.DummyPlayer;
import net.sssubtlety.dispenser_configurator.behavior.delegate.DoubleTypedDelegate;
import net.sssubtlety.dispenser_configurator.behavior.predicate.universal.DualSetUniversalAcceptor;
import org.apache.commons.lang3.mutable.MutableObject;

import java.util.List;

import static net.sssubtlety.dispenser_configurator.behavior.GenericDispenserBehavior.fillIfEmpty;
import static net.sssubtlety.dispenser_configurator.behavior.delegate.entity.EntityTypeDelegate.getPotentialEntities;

public abstract class BlockAndEntityDelegate extends DoubleTypedDelegate<Block, EntityType<?>> {
    @Override
    protected final Block tryCastA(Object obj) {
        return obj instanceof Block block ? block : null;
    }

    @Override
    protected final EntityType<?> tryCastB(Object obj) {
        return obj instanceof EntityType<?> entityType ? entityType : null;
    }

    @Override
    public final void assignMissingLists() {
        if (aDualSet == null) aDualSet = DualSetUniversalAcceptor.BLOCK_ACCEPTOR;
        if (bDualSet == null) bDualSet = DualSetUniversalAcceptor.ENTITY_TYPE_ACCEPTOR;
    }

    @Override
    public final boolean behaviorDelegation(BlockPointer dispenserPointer, Direction facing, DummyPlayer dummy, MutableObject<BlockHitResult> mutableBlockHitResult, MutableObject<List<LivingEntity>> mutableLivingEntityList) {
        BlockPos facingPos = getFacingPos(dispenserPointer, facing);

        BlockState facingBlockState = dispenserPointer.getWorld().getBlockState(facingPos);
        fillIfEmpty(mutableLivingEntityList, () -> getPotentialEntities(dispenserPointer, facing));

        if (this.allows(facingBlockState.getBlock()))
           return blockDelegationImplementation(dummy, dispenserPointer, facingBlockState);

        for (LivingEntity livingEntity : mutableLivingEntityList.getValue())
            if (this.allows(livingEntity.getType()) &&
                    entityDelegationImplementation(dummy, dispenserPointer, livingEntity))
                return true;

        return false;
    }

    protected abstract boolean blockDelegationImplementation(DummyPlayer dummy, BlockPointer dispenserPointer, BlockState facingBlockState);
    protected abstract boolean entityDelegationImplementation(DummyPlayer dummy, BlockPointer dispenserPointer, LivingEntity livingEntity);
}

