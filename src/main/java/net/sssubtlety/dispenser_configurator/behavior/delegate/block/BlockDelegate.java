package net.sssubtlety.dispenser_configurator.behavior.delegate.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.sssubtlety.dispenser_configurator.DummyPlayer;
import net.sssubtlety.dispenser_configurator.behavior.delegate.TypedDelegate;
import net.sssubtlety.dispenser_configurator.behavior.predicate.universal.DualSetUniversalAcceptor;
import org.apache.commons.lang3.mutable.MutableObject;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import static net.sssubtlety.dispenser_configurator.behavior.GenericDispenserBehavior.fillIfEmpty;

public abstract class BlockDelegate extends TypedDelegate<Block> {
    @Override
    public final boolean behaviorDelegation(BlockPointer dispenserPointer, Direction facing, DummyPlayer dummy, MutableObject<BlockHitResult> mutableBlockHitResult, MutableObject<List<LivingEntity>> mutableLivingEntityList) {
        final BlockPos facingPos = getFacingPos(dispenserPointer, facing);
        fillIfEmpty(mutableBlockHitResult, () -> getBlockHitResult(dispenserPointer, facing, facingPos));

        final BlockState facingBlockState = dispenserPointer.getWorld().getBlockState(facingPos);

        return this.allows(facingBlockState.getBlock()) &&
                behaviorDelegationImplementation(dummy, dispenserPointer, mutableBlockHitResult.getValue(), facingBlockState);
    }

    @NotNull
    public static BlockHitResult getBlockHitResult(BlockPointer dispenserPointer, Direction facing, BlockPos facingPos) {
        return new BlockHitResult(new Vec3d(dispenserPointer.getX(), dispenserPointer.getY(), dispenserPointer.getZ()), facing, facingPos, false);
    }

    protected abstract boolean behaviorDelegationImplementation(DummyPlayer dummy, BlockPointer dispenserPointer, BlockHitResult blockHitResult, BlockState facingBlockState);

    @Override
    protected Block tryCast(Object obj) {
        return obj instanceof Block block ? block : null;
    }

    @Override
    public final void assignMissingLists() {
        if (dualSet == null) dualSet = DualSetUniversalAcceptor.BLOCK_ACCEPTOR;
    }
}
