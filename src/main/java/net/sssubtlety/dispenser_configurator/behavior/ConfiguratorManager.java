package net.sssubtlety.dispenser_configurator.behavior;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Streams;
import com.google.gson.*;
import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;
import net.fabricmc.fabric.api.resource.ResourceReloadListenerKeys;
import net.minecraft.block.entity.DispenserBlockEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.resource.AutoCloseableResourceManager;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.profiler.Profiler;
import net.sssubtlety.dispenser_configurator.behavior.delegate.DispenserBehaviorDelegate;

import java.util.*;

import static net.minecraft.util.JsonHelper.getType;
import static net.sssubtlety.dispenser_configurator.DispenserConfigurator.*;

public class ConfiguratorManager extends JsonDataLoader implements IdentifiableResourceReloadListener {
    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
    public static final String PRIORITY_FALL_BACK_ERROR = "\"priority\" must be an integer, \"MAX\", or \"MIN\". Using default priority 0 instead.";

    public interface Keys {
        String targets = "targets";
        String priority = "priority";
        String MAX = "MAX";
        String MIN = "MIN";

        String items = "items";
        String block_allow_list = "block_allow_list";
        String block_deny_list = "block_deny_list";
        String entity_allow_list = "entity_allow_list";
        String entity_deny_list = "entity_deny_list";
        String block_white_list = "block_white_list";

        ImmutableList<String> block_allow_keys = ImmutableList.of(block_allow_list, block_white_list);
        String block_black_list = "block_black_list";
        ImmutableList<String> block_deny_keys = ImmutableList.of(block_deny_list, block_black_list);
        String entity_white_list = "entity_white_list";
        ImmutableList<String> entity_allow_keys = ImmutableList.of(entity_allow_list, entity_white_list);
        String entity_black_list = "entity_black_list";
        ImmutableList<String> entity_deny_keys = ImmutableList.of(entity_deny_list, entity_black_list);
        @SuppressWarnings("UnstableApiUsage")
        ImmutableList<String> all_list_keys = Streams.concat(
                block_allow_keys.stream(),
                block_deny_keys.stream(),
                entity_allow_keys.stream(),
                entity_deny_keys.stream()
        ).collect(ImmutableList.toImmutableList());
    }

    private static final String NON_STRING_ERROR_MSG = "Found non-string element in 'items'. Only strings are allowed. ";

    private static final PriorityBehaviorMultiMap BEHAVIOR_MAP = new PriorityBehaviorMultiMap();

    private static Map<Identifier, JsonElement> loader;

    public ConfiguratorManager(String dataType) {
        super(GSON, dataType);
    }

    // return true if vanilla behavior was replaced (should not be dispensed)
    public static boolean tryReplaceDispensation(BlockPointer blockPointer, ItemStack stack, int i) {
        final Collection<GenericDispenserBehavior> behaviors = BEHAVIOR_MAP.get(stack);
        if (behaviors != null) {
//        DispensationResult replaced = DispensationResult.NOT_REPLACED;
            for (GenericDispenserBehavior behavior : behaviors) {
                stack = behavior.dispenseSilently(blockPointer, stack);
                ((DispenserBlockEntity)blockPointer.getBlockEntity()).setStack(i, stack);
                if (behavior.isSuccess()) return true;
//            if (behavior.exclusive) replaced = DispensationResult.REPLACED;
            }
        }

        return false;
    }

    public static Configurator read(Identifier id, JsonObject jsonObject) {
        int priority = getPriority(jsonObject, id);

        Configurator configurator = new Configurator(id, priority);

        JsonArray targetElements = JsonHelper.getArray(jsonObject, Keys.targets, new JsonArray());
        // access is safe *[0]
        if (targetElements.size() > 0) {
            for (JsonElement target : targetElements) {
                // one or more target specified, use them
                if (target.isJsonPrimitive()) {
                    final String targetString = target.getAsString();
                    final DispenserBehaviorDelegate delegate = DispenserBehaviorDelegate.DELEGATE_FACTORY_MAP.get(targetString).get();

                    if (delegate == null)
                        LOGGER.error("Found unrecognized target: " + targetString);
                    else {
                        if (configurator.exclusive) {
                            final List<String> delegateNames = configurator.getDelegateNames();
                            LOGGER.warn("Found target after exclusive target: " + targetString +
                                    ". Exclusive target: " + delegateNames.get(delegateNames.size() - 1));
                        }
                        else configurator.addDelegate(delegate);
                    }
                }
            }
        } else { // if no targets specified, default to all targets
            DispenserBehaviorDelegate.DELEGATE_FACTORY_MAP.forEach((targetString, delegateSupplier) -> configurator.addDelegate(delegateSupplier.get()));
        }

        // itemStrings is handled separately because it's a required element
        JsonArray itemStrings = JsonHelper.getArray(jsonObject, Keys.items);
        if (itemStrings == null)
            LOGGER.error("Could not find required 'items' array in configurator '" + id + "'.");
        else if (itemStrings.size() < 1)
            LOGGER.error("'items' entry is empty in configurator '" + id + "'.");
        else {
            // itemStrings is populated
            for (JsonElement jsonElement : itemStrings)
                configurator.addToListFromString(Keys.items, getIdString(jsonElement));

            for (String list_key : Keys.all_list_keys) {
                JsonArray jsonArray = null;
                String foundKey = null;
                if (jsonObject.has(list_key)) {
                    jsonArray = JsonHelper.getArray(jsonObject, list_key);
                    foundKey = list_key;
                }

                if (foundKey != null) {
                    for (JsonElement listElement : jsonArray)
                        configurator.addToListFromString(foundKey, getIdString(listElement));
                } // these lists are optional, so if not found do nothing
            }
        }

        return configurator;
    }

    private static int getPriority(JsonObject jsonObject, Identifier id) {
        if (jsonObject.has(Keys.priority)) {
            JsonElement priorityElement = jsonObject.get(Keys.priority);
            if (priorityElement.isJsonPrimitive()) {
                JsonPrimitive priorityPrimitive = priorityElement.getAsJsonPrimitive();
                if (priorityPrimitive.isNumber()) {
                    int priority = priorityPrimitive.getAsInt();
                    if (priority > Configurator.MAX_PRIORITY) {
                        LOGGER.warn("\"priority\" in " + NAMESPACE + " " + id +
                                Configurator.GREATER_THAN_MAX_FALLBACK);
                        return Configurator.MAX_PRIORITY;
                    } else if (priority < Configurator.MIN_PRIORITY) {
                        LOGGER.warn("\"priority\" in " + NAMESPACE + " " + id +
                                Configurator.LESS_THAN_MIN_FALLBACK);
                        return Configurator.MIN_PRIORITY;
                    } else return priority;
                } else {
                    if (priorityPrimitive.isString()) {
                        String priorityString = priorityPrimitive.getAsString();
                        if (priorityString.equals(Keys.MAX)) return Configurator.MAX_PRIORITY;
                        else if (priorityString.equals(Keys.MIN)) return Configurator.MIN_PRIORITY;
                        else LOGGER.error("Found unrecognized \"priority\" string in " + NAMESPACE + " " +
                                    id + ": " + priorityString + ". " + PRIORITY_FALL_BACK_ERROR);
                    } else {
                         LOGGER.error("Found unrecognized type for \"priority\" in " + NAMESPACE + " " +
                                id + ": " + getType(priorityPrimitive) + ". " + PRIORITY_FALL_BACK_ERROR);
                    }
                }
            } else {
                LOGGER.error("Found unrecognized type for \"priority\" in " + NAMESPACE + " " +
                        id + ": " + getType(priorityElement) + ". " + PRIORITY_FALL_BACK_ERROR);
            }
        }
        return 0;
    }

    private static String getIdString(JsonElement element) {
        if (element.isJsonPrimitive()) {
            JsonPrimitive primitiveString = element.getAsJsonPrimitive();
            if (primitiveString.isString())
                return element.getAsString();
            else LOGGER.error(NON_STRING_ERROR_MSG);
        } else LOGGER.error(NON_STRING_ERROR_MSG);
        return null;
    }

    @Override
    public Identifier getFabricId() {
        return MOD_ID;
    }

    @Override
    public Collection<Identifier> getFabricDependencies() {
        return Collections.singleton(ResourceReloadListenerKeys.TAGS);
    }

    @Override
    protected void apply(Map<Identifier, JsonElement> loader, ResourceManager manager, Profiler profiler) {
        ConfiguratorManager.loader = loader;
    }

    public static void postTagBinding() {
        if (loader == null) {
            LOGGER.error("loader not set in postLoad!");
            return;
        }
        PriorityBehaviorMultiMap.Builder mapBuilder = PriorityBehaviorMultiMap.builder();
        loader.forEach((identifier, element) -> {
            if (element.isJsonObject()) {
                try {
                    mapBuilder.createMappedBehavior(read(identifier, element.getAsJsonObject()));
                } catch (Exception e) {
                    LOGGER.error(e.getMessage());
                }
            }
        });
        BEHAVIOR_MAP.reset(mapBuilder);
        loader = null;
    }
}