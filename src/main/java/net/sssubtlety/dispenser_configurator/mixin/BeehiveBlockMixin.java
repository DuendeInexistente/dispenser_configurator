package net.sssubtlety.dispenser_configurator.mixin;

import net.minecraft.block.BeehiveBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.List;

@Mixin(BeehiveBlock.class)
public abstract class BeehiveBlockMixin {
    @Inject(method = "angerNearbyBees", cancellable = true, locals = LocalCapture.CAPTURE_FAILEXCEPTION, at = @At(value = "INVOKE_ASSIGN", target = "Ljava/util/List;size()I"))
    void onGetPlayerListSize(World world, BlockPos pos, CallbackInfo ci, List list, List list2) {
        if (list2.isEmpty()) ci.cancel();
    }
}
