package net.sssubtlety.dispenser_configurator.mixin;

import net.minecraft.server.ServerReloadableResources;
import net.minecraft.util.registry.DynamicRegistryManager;
import net.sssubtlety.dispenser_configurator.behavior.ConfiguratorManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ServerReloadableResources.class)
public class ServerReloadableResourcesMixin {
    @Inject(
            method = "updateRegistryTags(Lnet/minecraft/util/registry/DynamicRegistryManager;)V",
            at = @At("TAIL")
    )
    private void dispenser_configurator$postTagUpdate(DynamicRegistryManager dynamicRegistryManager, CallbackInfo ci) {
        ConfiguratorManager.postTagBinding();
    }
}
