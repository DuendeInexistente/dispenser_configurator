package net.sssubtlety.dispenser_configurator;

import net.minecraft.util.Identifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class DispenserConfigurator {
	public static final String NAMESPACE = "dispenser_configurator";
	public static final Identifier MOD_ID = new Identifier(NAMESPACE, NAMESPACE);

	public static final Logger LOGGER = LogManager.getLogger();
}
