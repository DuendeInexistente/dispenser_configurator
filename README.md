This mod is for the [Fabric mod loader](https://www.fabricmc.net/) and [![minecraft versions](https://cf.way2muchnoise.eu/versions/Minecraft_dispenser-configurator_all.svg)](https://www.curseforge.com/minecraft/mc-mods/dispenser-configurator/files). 

Download the mod on [CurseForge](https://www.curseforge.com/minecraft/mc-mods/dispenser-configurator) or [Modrinth](https://modrinth.com/mod/dispenser-configurator)!

This mod lets you configure the vanilla Dispenser to work with any item's use (right-click) functionality. 

Configuration is done via a new data type loaded through datapacks: `"dispenser_configurator"`.

In a `"dispenser_configurator"`, you can define a list of items and item tags you wish to have functionality in Dispensers. 
Minecraft has several ways of handling item usage (5), so a `"dispenser_configurator"` can also have a `"target"` list that defines how it should use its items. 

The mod includes the "Default Dispenser Configurators" datapack that contains several configurators I've tested and that I think fit well into vanilla+ minecraft. You can disable the datapack using the normal `/datapack disable` command.

More information about how to create a `"dispenser_configurator"` is available on [the wiki](https://gitlab.com/supersaiyansubtlety/dispenser_configurator/-/wikis/home).
The builtin "Default Dispenser Configurators" datapack also serves as a good example datapack.

Known issues: 
- `"ITEM_USE"` doesn't behave desirably for all items in all situations. 

![[Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api/files/all?filter-game-version=1738749986:70886)](https://i.imgur.com/Ol1Tcf8.png)

This mod is only for Fabric and I won't be porting it to Forge. The license is [MIT](https://will-lucic.mit-license.org/), however, so anyone else is free to port it.

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
